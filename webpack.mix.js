let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js');
mix.js('resources/assets/js/wait.js', 'public/js');


mix.sass('resources/assets/scss/main.scss', 'public/css/app.css');

mix.copyDirectory('resources/assets/fonts', 'public/fonts')
    .copyDirectory('resources/assets/img', 'public/img');
