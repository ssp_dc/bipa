<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Die BIPA Fotobox</title>
    <meta property="og:url" content="https://fotobox.bipa.at/app"/>
    <meta property="og:type" content="website"/>
    <meta property="og:title" content="Die BIPA Fotobox"/>
    <meta property="og:description"
          content="Verrate jetzt allen, was dich zum Mädchen macht und gestalte dein eigenes Bild."/>

    <meta property="og:image" content="https://fotobox.bipa.at/assets/img/fb-share-poster.jpg"/>

    <!-- build:css assets/css/main.css -->
    <link rel="stylesheet" type="text/css" href=".{{ mix('/css/app.css') }}">
    <!-- endbuild -->

</head>
<body class="welcome">

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-49581674-5"></script>

<script>
    window.dataLayer = window.dataLayer || [];
    function gtag() {
        dataLayer.push(arguments);
    }
    gtag('js', new Date());

    gtag('config', 'UA-49581674-5');
</script>
<script>
    window.fbAsyncInit = function () {
        FB.init({
            appId: '181754949056583',
            cookie: true,
            autoLogAppEvents: true,
            xfbml: true,
            status: true,
            version: 'v2.10'
        });
        FB.AppEvents.logPageView();
    };

    (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {
            return;
        }
        js = d.createElement(s);
        js.id = id;
        js.src = "https://connect.facebook.net/de_AT/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>

<div class="background-assets">
    <div class="circles">
        <div class="circle pink large"></div>
        <div class="circle yellow large"></div>
        <div class="circle orange small"></div>
        <div class="circle blue small"></div>
        <div class="center">
            <div class="circle pink small"></div>
            <div class="circle yellow small"></div>
        </div>
    </div>

    <div class="photos">
        <div class="photo photo-1 fadeInUp animated"></div>
        <div class="photo photo-2 fadeInDown animated"></div>
        <div class="photo photo-3 fadeInRight animated"></div>
        <div class="photo photo-4 fadeInLeft animated"></div>
        <div class="photo photo-5 fadeInRight animated"></div>
        <div class="photo photo-6 fadeInLeft animated"></div>
    </div>
</div>

<a class="logo" href="#">Bipa</a>
<div class="content">
    <div class="container bipa-state state-welcome">
        <div class="row">
            <div class="col">
                <h2 class="font-weight-bold mb-2">Die BIPA Fotobox</h2>
                <p class="mb-3">
                    Verrate jetzt allen, was dich zum Mädchen macht und gestalte dein eigenes Bild.
                </p>
                <div class="bipa-terms d-flex mb-2">
                    <div>
                        <input type="checkbox" id="terms-checkbox"/>
                        <label for="terms-checkbox" class="text-left"></label>
                    </div>
                    <p class="text-left">Durch Aktivieren des Häkchens stimmen Sie der vorübergehenden Speicherung Ihres
                        Lichtbildes zum Zweck der Generierung eines Profilbildes im Format der BIPA Parfümerien GmbH
                        Kampagne gemäß unserer <a href="http://www.bipa.at/content/service-privacy.html"
                                                  target="_blank">Datenschutzrichtlinien</a> zu. Diese Zustimmung kann
                        jederzeit schriftlich durch Meldung an info@bipa.at widerrufen werden. Das generierte Profilbild
                        wird nach Fertigstellung nicht weiter gespeichert.</p>
                </div>

                <button id="enter-btn" class="bipa-btn bipa-btn-icon" disabled>Los geht's<span
                            class="bipa-icon arrow-right "></span></button>
            </div>
        </div>
    </div>

    <div class="container bipa-state state-choose-image">
        <div class="row">
            <div class="col-sm-12 col-lg-3 mb-3 mb-lg-0">
                <div class="d-none d-lg-block">
                    <h2 class="font-weight-bold">Die BIPA Fotobox</h2>
                    <p>
                        Verrate jetzt allen, was dich zum Mädchen macht und gestalte dein eigenes Bild.
                    </p>
                </div>

                <h2 class="font-weight-bold d-none d-lg-block">So einfach funktioniert's:</h2>
                <ol class="bipa-list">
                    <li class="active">Foto auswählen</li>
                    <li>Eigenschaft aussuchen</li>
                    <li>Bild teilen</li>
                </ol>
            </div>
            <div class="col-sm-12 col-lg-6">
                <div class="bipa-image-wrap d-flex justify-content-center align-items-center">
                    <ul class="bipa-btn-list">
                        <li>
                            <button id="take-picture" class="bipa-btn bipa-btn-icon bipa-btn-outline font-size-medium">
                                Foto aufnehmen<span class="bipa-icon camera-icon"></span></button>
                        </li>
                        <li class="bipa-file-input-wrap">
                            <input id="upload-image-input" class="bipa-file-input" type="file"
                                   accept="image/png, image/gif, image/jpg, image/jpeg, image/bmp">
                            <button class="bipa-btn bipa-btn-icon bipa-btn-outline font-size-medium">Foto hochladen<span
                                        class="bipa-icon arrow-up"></span></button>
                        </li>
                        <li>
                            <button id="facebook-photo-input"
                                    class="bipa-btn bipa-btn-icon bipa-btn-outline font-size-medium">Facebook
                                Foto<span class="bipa-icon fb"></span></button>
                        </li>
                    </ul>
                    <div class="camera-wrap"></div>
                </div>
                <div class="take-picture-btn-wrap text-center mt-3">
                    <button class="take-picture-btn bipa-btn bipa-btn-single-icon bipa-btn-round"><span
                                class="bipa-icon camera-icon"></span></button>
                </div>

            </div>
            <div class="col-sm-12 col-lg-3 d-none d-md-block">

            </div>
        </div>
    </div>

    <div class="container bipa-state state-edit-image">
        <div class="row">

            <div class="col-sm-12 col-lg-3 mb-3 mb-lg-0">
                <div class="d-none d-lg-block">
                    <h2 class="font-weight-bold">Die BIPA Fotobox</h2>
                    <p>
                        Verrate jetzt allen, was dich zum Mädchen macht und gestalte dein eigenes Bild.
                    </p>
                </div>

                <h2 class="font-weight-bold d-none d-lg-block">So einfach funktioniert's:</h2>
                <ol class="bipa-list">
                    <li>Foto auswählen</li>
                    <li class="active">Eigenschaft aussuchen</li>
                    <li>Bild teilen</li>
                </ol>
            </div>

            <div class="col-sm-12 col-lg-6 mb-3 mb-lg-0">
                <div class="bipa-image-wrap">
                    <canvas id="img-canvas" width="476" height="476"></canvas>

                    <div class="change-image-menu justify-content-center align-items-center">
                        <ul class="bipa-btn-list">
                            <li>
                                <button id="change-taken-picture"
                                        class="bipa-btn bipa-btn-icon bipa-btn-outline font-size-medium">Foto
                                    aufnehmen<span class="bipa-icon camera-icon"></span></button>
                            </li>
                            <li class="bipa-file-input-wrap">
                                <input id="change-image-input" class="bipa-file-input" type="file"
                                       accept="image/png, image/gif, image/jpg, image/jpeg, image/bmp">
                                <button class="bipa-btn bipa-btn-icon bipa-btn-outline font-size-medium">Foto
                                    hochladen<span class="bipa-icon arrow-up"></span></button>
                            </li>
                            <li>
                                <button id="change-facebook-photo-input"
                                        class="bipa-btn bipa-btn-icon bipa-btn-outline font-size-medium">Facebook
                                    Foto<span class="bipa-icon fb"></span></button>
                            </li>
                        </ul>
                        <div class="camera-wrap"></div>
                        <button id="close-change-image-menu"
                                class="bipa-btn bipa-btn-single-icon bipa-btn-outline font-size-medium"><span
                                    class="bipa-icon bipa-close"></span></button>
                    </div>
                </div>
                <div class="clearfix text-right change-image-wrap">
                    <button id="change-image" class="bipa-btn bipa-btn-plain float-lg-right p-0">Foto ändern</button>
                </div>
                <div class="take-picture-btn-wrap text-center mt-3">
                    <button class="take-picture-btn bipa-btn bipa-btn-single-icon bipa-btn-round"><span
                                class="bipa-icon camera-icon"></span></button>
                </div>


            </div>

            <div class="col-sm-12 col-lg-3 d-flex justify-content-center d-lg-block">
                <p class="font-weight-bold dropdown-text d-none d-lg-block">Wähle hier die Eigenschaft aus, die dich zum
                    Mädchen macht.</p>
                <div class="position-relative">

                    <div id="picture-templates-select" class="bipa-dropdown-wrap mx-auto">
                        <button class="bipa-btn bipa-btn-icon bipa-btn-outline font-size-medium">
                            <span>&nbsp;</span><span class="bipa-icon arrow-down"></span></button>
                        <div class="bipa-dropdown-menu">
                            <ul>
                                <li data-template="bunt">Bunt</li>
                                <li data-template="erfolgreich">Erfolgreich</li>
                                <li data-template="frech">Frech</li>
                                <li data-template="glucklich">Glücklich</li>
                                <li data-template="grossartig">Grossartig</li>
                                <li data-template="laut">Laut</li>
                                <li data-template="perfekt">Perfekt</li>
                                <li data-template="selbstbewusst">Selbstbewusst</li>
                                <li data-template="schon">Schön</li>
                                <li data-template="stark">Stark</li>
                                <li data-template="stolz">Stolz</li>
                                <li data-template="vielseitig">Vielseitig</li>
                                <li data-template="weltoffen">Weltoffen</li>
                                <li data-template="wild">Wild</li>
                                <li data-template="wunderbar">Wunderbar</li>
                            </ul>
                            <div class="dropdown-scrolldown">
                                <span class="scroll-up disabled"></span><span class="scroll-down"></span>
                            </div>
                        </div>
                    </div>

                    <select id="picture-templates-select-mobile">
                        <option value="bunt">Bunt</option>
                        <option value="erfolgreich">Erfolgreich</option>
                        <option value="frech">Frech</option>
                        <option value="glucklich">Glücklich</option>
                        <option value="grossartig">Grossartig</option>
                        <option value="laut">Laut</option>
                        <option value="perfekt">Perfekt</option>
                        <option value="selbstbewusst">Selbstbewusst</option>
                        <option value="schon">Schön</option>
                        <option value="stark">Stark</option>
                        <option value="stolz">Stolz</option>
                        <option value="vielseitig">Vielseitig</option>
                        <option value="weltoffen">Weltoffen</option>
                        <option value="wild">Wild</option>
                        <option value="wunderbar">Wunderbar</option>
                    </select>

                </div>
            </div>
            <div class="col-sm-12">
                <div id="image-share-btns-mobile" class="inline-btns d-lg-none mt-3 font-size-medium">
                    <a id="image-download-mobile" class="bipa-btn bipa-btn-single-icon" href="#" download
                       target="_blank"><span class="bipa-icon download"></span></a>
                    <button id="use-as-fb-pic-mobile" class="bipa-btn bipa-btn-single-icon"><span
                                class="bipa-icon fb"></span></button>
                </div>

                <div id="image-share-btns" class="inline-btns d-none d-lg-block mt-3">
                    <a id="image-download" class="bipa-btn bipa-btn-icon font-size-medium" href="#" download><span
                                class="label">Herunterladen</span><span class="bipa-icon download"></span></a>
                    <a id="use-as-fb-pic" class="bipa-btn bipa-btn-icon font-size-medium" href="#"
                       onclick="loginToFacebook()"><span
                                class="label">Als Profilbild verwenden</span><span class="bipa-icon fb"></span></a>
                </div>
            </div>
        </div>
    </div>

    <div class="container bipa-state state-thank">
        <div class="row">
            <div class="col-sm-12">
                <h2 class="font-weight-bold mb-2">Tolles Foto!</h2>
                <p class="mb-3">
                    Teile die BIPA Fotobox<br>mit deinen Freunden.
                </p>
            </div>
            <div class="inline-btns col-sm-12">
                <button onclick="bipa.changeState('choose-image')" class="bipa-btn bipa-btn-icon">Nochmal<span
                            class="bipa-icon refresh"></span></button>
                <button class="bipa-btn bipa-btn-icon"
                        onclick="window.open('https://www.facebook.com/sharer/sharer.php?u=https%3A//fotobox.bipa.at/app', 'popup', 'width=600,height=600')">
                    Fotobox teilen<span class="bipa-icon fb"></span></button>
            </div>
        </div>
    </div>
</div>
<!-- Markup for Carson Shold's Photo Selector -->
<div id="CSPhotoSelector">
    <div class="CSPhotoSelector_dialog">
        <div class="CSPhotoSelector_form">
            <div class="CSPhotoSelector_header">
                <a href="#" id="CSPhotoSelector_buttonClose">x</a>
                <p>Wählen Sie ein Foto aus</p>
            </div>
            <div class="CSPhotoSelector_content CSAlbumSelector_wrapper">
                <p>Durchsuchen Sie Ihre Fotoalben bis Sie ein Foto gefunden haben, das Sie verwenden möchten</p>
                <div class="CSPhotoSelector_searchContainer CSPhotoSelector_clearfix">
                    <div class="CSPhotoSelector_selectedCountContainer">Wählen Sie ein Album</div>
                </div>
                <div class="CSPhotoSelector_photosContainer CSAlbum_container"></div>
            </div>

            <div class="CSPhotoSelector_content CSPhotoSelector_wrapper">
                <p>Wählen Sie ein neues Foto</p>
                <div class="CSPhotoSelector_searchContainer CSPhotoSelector_clearfix">
                    <div class="CSPhotoSelector_selectedCountContainer"><span
                                class="CSPhotoSelector_selectedPhotoCount">0</span> / <span
                                class="CSPhotoSelector_selectedPhotoCountMax">0</span> Fotos ausgewählt
                    </div>
                    <a href="#" id="CSPhotoSelector_backToAlbums">Zurück zu Fotoalben</a>
                </div>
                <div class="CSPhotoSelector_photosContainer CSPhoto_container"></div>
            </div>
            <div id="CSPhotoSelector_loader"></div>
            <div class="CSPhotoSelector_footer CSPhotoSelector_clearfix">
                <a href="#" id="CSPhotoSelector_pagePrev" class="CSPhotoSelector_disabled">&laquo; Zurück</a>
                <a href="#" id="CSPhotoSelector_pageNext">Weiter &raquo;</a>
                <div class="CSPhotoSelector_pageNumberContainer">
                    <span id="CSPhotoSelector_pageNumber">1</span> / <span
                            id="CSPhotoSelector_pageNumberTotal">1</span>
                </div>
                <a href="#" id="CSPhotoSelector_buttonOK">Ok</a>
                <a href="#" id="CSPhotoSelector_buttonCancel">Abbrechen</a>
            </div>
        </div>
    </div>
</div>
<footer class="footer text-center"><a href="https://www.bipa.at/content/service-imprint.html"
                                      target="_blank">Impressum</a></footer>
<canvas id="snapshot-store" class="d-none"></canvas>
<!-- build:js assets/js/main.js -->
<script src=".{{ mix('/js/app.js') }}"></script>
<!-- endbuild -->

<!--   <div class="background-assets">
    <div class="photos">
      <div class="photo photo-1 fadeInUp animated"></div>
      <div class="photo photo-2 fadeInDown animated"></div>
      <div class="photo photo-3 fadeInRight animated"></div>
      <div class="photo photo-4 fadeInLeft animated"></div>
    </div>
    <div class="circles">
      <div class="circle pink large"></div>
      <div class="circle yellow large"></div>
      <div class="circle orange small"></div>
      <div class="circle blue small"></div>
      <div class="center">
        <div class="circle pink small"></div>
        <div class="circle yellow small"></div>
      </div>
    </div>
  </div> -->

</body>
</html>
