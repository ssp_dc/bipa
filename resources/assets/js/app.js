require('./bootstrap');
require('./photoselector');

const $cameraEl = $(`
<video id="video" class="take-picture-video" muted>
  Video stream not available.
</video>
`);


navigator.getUserMedia = navigator.getUserMedia ||
    navigator.webkitGetUserMedia ||
    navigator.mozGetUserMedia ||
    navigator.msGetUserMedia;

// if (!navigator.getUserMedia || window.location.protocol != "https:") {
//   // $('body').addClass('no-navigator');
// } else {
//   // navigator.getUserMedia({video: true}, function() {
//   // // webcam is available
//   // }, function() {
//   //   $('body').addClass('no-navigator');
//   // });
// }

/**
 * Determine the mobile operating system.
 * This function returns one of 'iOS', 'Android', 'Windows Phone', or 'unknown'.
 *
 * @returns {String}
 */
function isMobile() {
    var userAgent = navigator.userAgent || navigator.vendor || window.opera;

    // Windows Phone must come first because its UA also contains "Android"
    if (/windows phone/i.test(userAgent)) {
        return "Windows Phone";
    }

    if (/android/i.test(userAgent)) {
        return "Android";
    }

    // iOS detection from: http://stackoverflow.com/a/9039885/177710
    if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
        return "iOS";
    }

    return false;
}

const mobileOs = isMobile();

class BipaApp {
    constructor() {
        this.state = 'welcome';

        $('body').addClass(mobileOs ? 'bipa-mobile' : 'bipa-desktop');


        $('#enter-btn').on('click', (e) => {
            $('#terms-checkbox').is(':checked') && this.changeState('choose-image');
        });

        $('#terms-checkbox').on('change', function () {
            $('#enter-btn').prop('disabled', !$(this).is(':checked'));
        });

        $('#upload-image-input').on('change', (e) => {
            this.changeState('edit-image');
        });

        $('#picture-templates-select button').on('click', (e) => {
            $('#picture-templates-select').toggleClass('active');
        });

        $('#change-image').on('click', function (e) {
            $(this)
                .parent()
                .siblings('.bipa-image-wrap')
                .addClass('changing-img');
        });

        $('#close-change-image-menu').on('click', function (e) {
            // $(this).closest('.bipa-image-wrap')
            // 	.removeClass('changing-img');
            $('.changing-img').removeClass('changing-img');
        });


        $('#picture-templates-select-mobile').on('change', function (e) {
            const $el = $(this);
            $el.siblings('.bipa-dropdown-wrap')
                .find('.bipa-btn span:eq(0)')
                .text($el.val());
        })

        $('#picture-templates-select li').on('click', function (e) {
            const $el = $(this);
            $el.closest('.bipa-dropdown-wrap')
                .find('.bipa-btn span:eq(0)')
                .text($el.text());
        });

        $('#image-download, #image-download-mobile').on('click', (e) => {
            this.changeState('thank');
        });

        $('.take-picture-btn-wrap .bipa-btn').on('click', (e) => {
            this.changeState('edit-image');
        })
    }

    changeState(newState) {
        $('body').removeClass(this.state).addClass(newState);
        this.state = newState;
    }
}

class BipaImage {
    constructor() {
        this.$canvasEl = $('#img-canvas');
        this.canvasEl = this.$canvasEl[0];
        this.canvas = new fabric.StaticCanvas('img-canvas');
        this.stream = null;
        this.video = null;
        this.currentHeadlineTemplate = null;
        this.currentBackground = null;
        this.curremtTemplateName = '';

        $('#upload-image-input, #change-image-input').on('change', (e) => {
            this.uploadImg(e);
        });

        $('#picture-templates-select-mobile').on('change', (e) => {
            this.onHeadlineSelect(e);
        });

        $('#picture-templates-select li').on('click', (e) => {
            $(e.target).closest('.bipa-dropdown-wrap').removeClass('active');
            this.onHeadlineSelect(e);
        });

        $('#image-download').on('click', (e) => {
            this.onDownload();
        });

        $('#take-picture').on('click', (e) => {
            if (window.location.protocol != "https:") {
                alert("Benutze HTTPS protokoll");
                return;
            }
            if (navigator && navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
                $(e.target).closest('.bipa-state')
                    .addClass('taking-picture');
                this.initCamera('choose-image');
            } else {
                alert('Dein Browser unterstützt die Kamerafunktion leider nicht');
            }
        });

        $('#change-taken-picture').on('click', (e) => {
            if (window.location.protocol != "https:") {
                alert("Benutze HTTPS protokoll");
                return;
            }
            if (navigator && navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
                $(e.target).closest('.bipa-state')
                    .addClass('taking-picture');
                this.initCamera('edit-image');
            } else {
                alert('Dein Browser unterstützt die Kamerafunktion leider nicht');
            }
        });
    }

    uploadImg(e) {
        if (e.target.files &&
            e.target.files[0] &&
            e.target.files[0].name.match(/\.(jpg|jpeg|png|gif|bmp)$/i)) {
            var reader = new FileReader();
            reader.onload = (event) => {
                var imgObj = new Image();
                imgObj.src = event.target.result;
                imgObj.onload = () => {
                    this.setBackground(imgObj);
                }
            }
            reader.readAsDataURL(e.target.files[0]);
        } else {
            alert('Ungültige Bilddatei, bitte versuche es erneut!');
        }

    }

    setImg(remoteSrc) {
        var imgObj = new Image();
        imgObj.crossOrigin = 'Anonymous';
        imgObj.src = remoteSrc;
        imgObj.onload = () => {
            this.setBackground(imgObj);
        }
    }

    sendImg(popupWindow) {
        const dataURL = this.canvas.toDataURL({
            format: 'jpg',
            quality: 1
        });

        var dataURLtoBlob = function (d) {
            var arr = d.split(','), mime = arr[0].match(/:(.*?);/)[1],
                bstr = atob(arr[1]), n = bstr.length, u8arr = new Uint8Array(n);
            while (n--) {
                u8arr[n] = bstr.charCodeAt(n);
            }
            return new Blob([u8arr], {type: mime});
        }

        var fd = new FormData();
        fd.append('photo', dataURLtoBlob(dataURL));

        $.ajax({
            type: "POST",
            url: "./photo",
            processData: false,
            contentType: false,
            data: fd
        }).done(function (response) {
            //just hope for the best
            popupWindow.postMessage(response, `${window.location.protocol}//${window.location.hostname}`);
        }).fail(function (jqXHR, textStatus) {
            // Upload has failed
            // Some error message would be nice.
            // console.log(jqXHR, textStatus);
        }).always(function () {
            bipa.changeState('thank');
        });
    }

    setUpDownload() {
        const els = $('#image-download, #image-download-mobile');
        els.each((idx, el) => {
            el.href = this.canvas.toDataURL({
                format: 'jpg',
                quality: 1
            });
            el.download = `BIPA_fotobox_ich-bin-${this.curremtTemplateName}`;

            if (mobileOs === 'iOS') {
                el.target = '_blank';
            }
        });
    }

    onDownload() {
        this.canvas.remove(this.currentBackground);
        this.canvas.remove(this.currentHeadlineTemplate);
        this.currentHeadlineTemplate = null;
        this.currentBackground = image;
        this.curremtTemplateName = '';
        this.canvas.renderAll();

        $('.image-ready').removeClass('image-ready');

        $('.state-edit-image .bipa-list li').removeClass('active')
            .eq(1)
            .addClass('active');
    }

    initCamera(inState) {
        const $cameraWrap = $(`.state-${inState} .camera-wrap`);
        const $takePictureBtn = $(`.state-${inState} .take-picture-btn`);
        const $video = $cameraEl.clone();
        $cameraWrap.append($video);
        this.video = $video[0];
        // this.video = document.querySelector('video')

        $video.one('canplay', (e) => {
            this.video.setAttribute('width', this.video.videoWidth);
            this.video.setAttribute('height', this.video.videoHeight);
            this.scaleVideo($video);
            this.video.play();
            // $video.closest('.bipa-state').addClass('taking-picture');
            $cameraWrap.closest('.bipa-state').addClass('canplay');
            // alert('video should play');
        });

        $(window).on('resize.bipaVideo, orientationchange.bipaVideo', (e) => {
            this.scaleVideo($video);
        });

        $takePictureBtn.one('click', (e) => {
            this.video.pause();

            const snapshotCanvas = $('#snapshot-store')[0];
            const snapshotCtx = snapshotCanvas.getContext('2d');
            const imgObj = new Image();

            snapshotCanvas.width = this.video.videoWidth;
            snapshotCanvas.height = this.video.videoHeight;
            snapshotCtx.drawImage(this.video, 0, 0, this.video.videoWidth, this.video.videoHeight);

            imgObj.onload = () => {
                this.setBackground(imgObj);
                this.clearCameraInitMarkup();
                $video.remove();
                this.video = null;
                if (this.stream) {
                    this.stream.getVideoTracks()[0].stop();
                    this.stream = null;
                }
            }
            imgObj.src = snapshotCanvas.toDataURL('image/png');
            e.preventDefault();
        });

        navigator.mediaDevices.getUserMedia({
            video: true,
            audio: false
        }).then((stream) => {
            this.stream = stream;
            $video.closest('.bipa-state').addClass('taking-picture');
            this.video.srcObject = stream;
            // this.video.play();
        }).catch((err) => {
            // console.log("An error occured! " + err.name);
            switch (err.name) {
                case 'DevicesNotFoundError':
                    alert('Es konnte keine Kamera gefunden werden!');
                    break;
                case 'PermissionDeniedError':
                    alert('Kamerazugriff nicht möglich.');
                    break;
                default:
                    console.log(err);
            }
        });
    }

    scaleVideo($video) {
        const ratios = this.scaleRatios(this.video);
        const parentWidth = $video.closest('.bipa-image-wrap').width()
        const parentRatio = Math.min(parentWidth / 476, 1);

        const scaledWidth = this.video.videoWidth * ratios.x;
        const scaledHeight = this.video.videoHeight * ratios.y;

        const viewWidth = this.video.videoWidth * ratios.x * parentRatio;
        const viewHeight = this.video.videoHeight * ratios.y * parentRatio;

        $video.width(viewWidth);
        $video.height(viewHeight);

        if (scaledWidth > 476) {
            $video.css({left: Math.floor((parentWidth - viewWidth) / 2)});
        }

        if (scaledHeight > 476) {
            $video.css({top: Math.floor((parentWidth - viewHeight) / 2)});
        }
    }

    clearCameraInitMarkup() {
        $('.taking-picture').removeClass('taking-picture');
        $('.canplay').removeClass('canplay');
        $(window).off('resize.bipaVideo');
        $(window).off('orientationchange.bipaVideo');
    }

    onHeadlineSelect(e) {
        const $el = $(e.target);
        const templateName = $el.val() || $el.attr('data-template');

        this.setHeadlineTemplate(templateName);
        this.curremtTemplateName = templateName;

        $el.closest('.bipa-state')
            .addClass('image-ready')
            .find('.bipa-list li')
            .removeClass('active')
            .eq(2)
            .addClass('active');
    }

    setHeadlineTemplate(templateSlug) {
        fabric.Image.fromURL(
            `./img//templates/template_${templateSlug}.png`,
            (img) => {
                img.set({
                    left: 0,
                    top: 0,
                    selectable: false
                });
                this.canvas.remove(this.currentHeadlineTemplate);
                this.canvas.add(img)
                this.currentHeadlineTemplate = img;
                this.setUpDownload();
            }, {crossOrigin: true});
    }

    setBackground(imgObj) {
        $('.changing-img').removeClass('changing-img');
        var ratios = this.scaleRatios(imgObj);
        var image = new fabric.Image(imgObj);

        // console.log(imgObj, image.width, ratios)

        image.set({
            scaleX: ratios.x,
            scaleY: ratios.y,
            // width: 476,
            // height: 476
        });
        // console.log(image.width)

        this.canvas.centerObject(image);
        this.canvas.remove(this.currentBackground);
        this.canvas.remove(this.currentHeadlineTemplate);
        this.canvas.add(image);
        this.currentHeadlineTemplate && this.canvas.add(this.currentHeadlineTemplate);
        this.canvas.renderAll();

        this.currentBackground = image;
        this.setUpDownload();
    }

    scaleRatios(imgObj) {
        const initW = imgObj.videoWidth || imgObj.width;
        const initH = imgObj.videoHeight || imgObj.height;
        const ratio = initH / initW;

        let imgWidth = 476;
        let imgHeight = 476 * ratio;

        if (imgHeight < 476) {
            imgHeight = 476;
            imgWidth = imgHeight / ratio;
        }

        return {
            x: Math.ceil(imgWidth) / initW,
            y: Math.ceil(imgHeight) / initH
        };
    }
}


window.image = new BipaImage();
window.bipa = new BipaApp();

(function () {

    var scroll = 0,
        limit,
        $dropdown = $(".bipa-dropdown-menu ul"),
        $btn = $(".bipa-dropdown-wrap .bipa-btn"),
        itemCount = $dropdown.find('li').length,
        $su = $(".bipa-dropdown-menu .dropdown-scrolldown .scroll-up"),
        $sd = $(".bipa-dropdown-menu .dropdown-scrolldown .scroll-down");


    $(window).on('resize', function () {
        $dropdown.scrollTop(0);
    });

    $dropdown.scroll(function (e) {
        scroll = $(this).scrollTop();
        scroll <= 0 ? $su.addClass("disabled") : $su.removeClass("disabled");
        scroll >= limit ? $sd.addClass("disabled") : $sd.removeClass("disabled");

    });

    $(document).on("click", ".bipa-dropdown-menu ul li", function () {
        //$("#picture-templates-select").removeClass("active");
    });

    $(document).on("click", ".bipa-dropdown-menu .dropdown-scrolldown .scroll-down", function (e) {
        var step = $btn.outerHeight() - 1;
        console.log(step);
        limit = (itemCount - 5) * step;
        e.preventDefault();
        if (scroll < limit) {
            scroll = scroll - (scroll % step) + 2 * step;
            $dropdown.finish().animate({
                scrollTop: scroll
            });
        }
    });
    $(document).on("click", ".bipa-dropdown-menu .dropdown-scrolldown .scroll-up", function (e) {
        var step = $btn.outerHeight() - 1;
        console.log(step);
        e.preventDefault();
        if (scroll > 0) {
            scroll = scroll - (scroll % step) - 2 * step;
            $dropdown.finish().animate({
                scrollTop: scroll
            });
        }
    });

})();

