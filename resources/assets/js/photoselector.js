$(document).ready(function () {
    require('./vendor/photoselector');
    var selector, logActivity, callbackAlbumSelected, callbackPhotoUnselected, callbackSubmit;
    var buttonOK = $('#CSPhotoSelector_buttonOK');
    var o = this;


    /* --------------------------------------------------------------------
     * Photo selector functions
     * ----------------------------------------------------------------- */

    fbphotoSelect = function(id) {
        // if no user/friend id is sent, default to current user
        if (!id) id = 'me';

        callbackAlbumSelected = function(albumId) {
            var album, name;
            album = CSPhotoSelector.getAlbumById(albumId);
            // show album photos
            selector.showPhotoSelector(null, album.id);
        };

        callbackAlbumUnselected = function(albumId) {
            var album, name;
            album = CSPhotoSelector.getAlbumById(albumId);
        };

        callbackPhotoSelected = function(photoId) {
            var photo;
            photo = CSPhotoSelector.getPhotoById(photoId);
            buttonOK.show();
            logActivity('Selected ID: ' + photo.id);
        };

        callbackPhotoUnselected = function(photoId) {
            var photo;
            album = CSPhotoSelector.getPhotoById(photoId);
            buttonOK.hide();
        };

        callbackSubmit = function(photoId) {
            var photo;
            photo = CSPhotoSelector.getPhotoById(photoId);
            if (typeof photo === 'undefined' || !photo) {
                bipa.changeState('edit-image');
            } else {
                image.setImg(photo.source);
                bipa.changeState('edit-image');
                logActivity('Photo source:' + photo.source);
            }
        };


        // Initialise the Photo Selector with options that will apply to all instances
        CSPhotoSelector.init({debug: true});

        // Create Photo Selector instances
        selector = CSPhotoSelector.newInstance({
            callbackAlbumSelected	: callbackAlbumSelected,
            callbackAlbumUnselected	: callbackAlbumUnselected,
            callbackPhotoSelected	: callbackPhotoSelected,
            callbackPhotoUnselected	: callbackPhotoUnselected,
            callbackSubmit			: callbackSubmit,
            maxSelection			: 1,
            albumsPerPage			: 6,
            photosPerPage			: 64,
            autoDeselection			: true
        });

        // reset and show album selector
        selector.reset();
        selector.showAlbumSelector(id);
    }


    /* --------------------------------------------------------------------
     * Click events
     * ----------------------------------------------------------------- */

    $("#facebook-photo-input, #change-facebook-photo-input").click(function (e) {
        e.preventDefault();
        FB.login(function (response) {
            if (response.authResponse) {
                fbphotoSelect();
            } else {
                console.log('Not logged in');
            }
        }, {scope:'user_photos'});
    });

    // $(".photoSelect").click(function (e) {
    //     e.preventDefault();
    //     fbphotoSelect();
    // });

    logActivity = function (message) {
        console.log(message);
    };
});