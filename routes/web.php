<?php

Route::get('/', 'SupportController@home')->name('home');

Route::post('/photo', 'PhotoController@upload');
Route::get('/wait-for-upload-to-finish', 'PhotoController@wait');

Route::group(['prefix' => 'facebook'], function () {
    Route::get('obtain-permission', 'FacebookController@login')->name('facebook.login');
    Route::get('callback', 'FacebookController@callback')->name('facebook.callback');
    Route::get('upload', 'FacebookController@upload')->name('facebook.upload');
});

Route::fallback('SupportController@redirectHome');
