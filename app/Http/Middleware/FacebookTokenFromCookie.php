<?php

namespace App\Http\Middleware;

use Closure;
use Facebook\Exceptions\FacebookSDKException;
use SammyK\LaravelFacebookSdk\LaravelFacebookSdk;

class FacebookTokenFromCookie
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $facebook = resolve(LaravelFacebookSdk::class);
        try {
            $token = $facebook->getJavaScriptHelper()->getAccessToken();
        } catch (FacebookSDKException $e) {
            dd($e->getMessage());
        }

        //TODO: do something with token

        return $next($request);
    }
}
