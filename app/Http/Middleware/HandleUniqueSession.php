<?php

namespace App\Http\Middleware;

use Closure;
use Webpatser\Uuid\Uuid;

class HandleUniqueSession
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (! $request->session()->exists('client_id')) {
            $request->session()->put('client_id', (string) Uuid::generate(4));
        }

        return $next($request);
    }
}
