<?php

namespace App\Http\Controllers;

class SupportController extends Controller
{

    public function home()
    {
        return view('welcome');
    }


    public function redirectHome()
    {
        return redirect()->route('home');
    }
}
