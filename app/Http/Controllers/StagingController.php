<?php

namespace App\Http\Controllers;

class StagingController extends Controller
{

    public function destroySession()
    {
        session()->remove('client_id');
        session()->remove('facebook_token');
        session()->regenerate(true);

        return redirect()->route('home');
    }
}
