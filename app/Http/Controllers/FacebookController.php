<?php

namespace App\Http\Controllers;

use App\Adaptors\Facebook\Adaptor as Facebook;

class FacebookController extends Controller
{

    public function login(Facebook $facebook)
    {
        return redirect($facebook->loginUrl());
    }


    public function callback(Facebook $facebook)
    {
        $token = $facebook->handleCallback();

        session()->put('facebook_token', $token->getValue());

        return redirect()->route('facebook.upload');
    }


    public function upload(Facebook $facebook)
    {
        $file = storage_path('app/public/photos/'.date('Y-m-d').'/'.filename());

        if (file_exists($file)) {
            $photoId = $facebook->uploadPicture($file);
        } else {
            abort(500, 'Something went wrong.');
        }

        return redirect($facebook->makeProfilePicture($photoId));
    }
}
