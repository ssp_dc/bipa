<?php

namespace App\Http\Controllers;

use App\Http\Requests\PhotoRequest;

class PhotoController extends Controller
{

    public function upload(PhotoRequest $request)
    {
        $request->file('photo')->storeAs('public/photos/'.date('Y-m-d'), filename());

        return response()->json([
            'message' => 'Photo uploaded.',
            'file'    => asset('storage/photos/'.date('Y-m-d').'/'.filename()),
            'popup'   => route('facebook.login'),
        ], 201);
    }

    public function wait() {
        return view('wait');
    }
}
