<?php

namespace App\Adaptors\Facebook;

use Facebook\Exceptions\FacebookSDKException;
use SammyK\LaravelFacebookSdk\LaravelFacebookSdk;

class Adaptor
{

    /**
     * @var \SammyK\LaravelFacebookSdk\LaravelFacebookSdk
     */
    private $facebookSdk;


    function __construct(LaravelFacebookSdk $facebookSdk)
    {
        $this->facebookSdk = $facebookSdk;
        if (session()->exists('facebook_token')) {
            $this->facebookSdk->setDefaultAccessToken(session()->get('facebook_token'));
        }
    }


    public function loginUrl()
    {
        return $this->facebookSdk->getLoginUrl(['user_photos', 'publish_actions'], route('facebook.callback', false));
    }


    public function handleCallback()
    {
        try {
            $token = $this->facebookSdk->getAccessTokenFromRedirect();
        } catch (FacebookSDKException $e) {
            dd($e->getMessage());
        }

        if ( ! $token) {
            abort(403, 'In order to post the photo to Facebook you need to grant us permission to upload photos.');
        }

        if ( ! $token->isLongLived()) {
            // OAuth 2.0 client handler
            $oauthClient = $this->facebookSdk->getOAuth2Client();

            // Extend the access token.
            try {
                $token = $oauthClient->getLongLivedAccessToken($token);
            } catch (FacebookSDKException $e) {
                abort(403, 'In order to post the photo to Facebook you need to grant us permission to upload photos.');
            }
        }

        $this->facebookSdk->setDefaultAccessToken($token);

        return $token;
    }


    /**
     * @param $filePath
     *
     * @return integer | id of photo that was uploaded
     */
    public function uploadPicture($filePath)
    {
        $data = [
            'source'  => $this->facebookSdk->fileToUpload($filePath),
        ];

        try {
            $response = $this->facebookSdk->post('/me/photos', $data);
        } catch (FacebookSDKException $e) {
            abort(403, 'In order to post the photo to Facebook you need to grant us permission to upload photos.');
        }

        return $response->getGraphNode()['id'];
    }


    public function makeProfilePicture($photoId)
    {
        return 'https://m.facebook.com/photo.php?fbid='.$photoId.'&prof';
    }
}