<?php
if ( ! function_exists('filename')) {
    function filename()
    {
        return session()->get('client_id').'.jpg';
    }
}
